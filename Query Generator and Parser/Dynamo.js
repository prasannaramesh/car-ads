const { query } = require("express");
const uuid = require("uuid");

/**
 * Create queries based on the schema and databse operation
 */
class Dynamo {
    #queryParameters; #schema;

    constructor(tableName, schema) {
        this.#schema = schema;
        this.#initializeParams(tableName);
    }

    #initializeParams = (tableName) => {
        this.#queryParameters = {
            TableName: tableName,
        }
    }

    /**
     * Convert value from one type to another according to schema
     * @param {primitives} type 
     * @param {*} data 
     */
    #typeConverter = (type, data) => {
        switch (type) {
            case Number:
                return Number(data);
            case Boolean:
                return Boolean(data);
            case Date:
                return new Date(data).getTime();
            case String:
                return data.toSting();
            default:
                return data;
        }
    }

    /**
     * Create parameters based on the type of databse operation
     * @param {string} type 
     * @param {string} attribute 
     * @param {*} attributeValue 
     * @param {string} id 
     */
    #generateParameters = (type, attribute, attributeValue, id) => {
        switch (type) {
            case "CREATE":
                if (this.#queryParameters.Item === undefined) { // Initialization of create parameter
                    this.#queryParameters.Item = {
                        guid: uuid.v1(),//`AS-${new Date().toJSON().replace(/[-T:.Z]/g, "")}`,
                    }
                }
                this.#queryParameters.Item[attribute] = attributeValue;
                break;
            case "UPDATE":
                if (this.#queryParameters.Key === undefined) { // Initialization of update parameter
                    this.#queryParameters.Key = {
                        guid: id
                    }
                    this.#queryParameters.UpdateExpression = "set";
                    this.#queryParameters.ExpressionAttributeValues = {};
                    this.#queryParameters.ExpressionAttributeNames = {};
                    this.#queryParameters.ReturnValues = "ALL_NEW"
                }
                let name = attribute.replace(" ", "_");
                this.#queryParameters.UpdateExpression += this.#queryParameters.UpdateExpression.length === 3 ? ` #${name} = :${name}` : `, #${name} = :${name}`;
                this.#queryParameters.ExpressionAttributeValues[`:${name}`] = attributeValue;
                this.#queryParameters.ExpressionAttributeNames[`#${name}`] = attribute;
                break;
            default:
                break;
        }
    }

    /**
     * generates error details based on attribute value
     * @param {string} attribute 
     * @param {*} attributeValue 
     * @param {string} attributeType 
     * @param {string} additionalMessage
     */
    #errorHandler = (attribute, attributeValue, attributeType, additionalMessage) => {
        let error;
        if (attributeValue !== undefined) {
            error = {
                statusCode: 422,
                name: "ValidationError",
                message: `Unprocessable Entity - ${attribute}. Expecting ${attributeType}. ${additionalMessage || ""}`,
            };
        }
        else {
            error = {
                statusCode: 400,
                name: "MissingField",
                message: `Missing field - ${attribute} of type ${attributeType}. ${additionalMessage || ""}`
            };
        }
        return error;
    }

    /**
     * Promised based validation of all input objects
     * @param {object} body 
     * @param {string} id 
     */
    validate(body, id) {
        this.#initializeParams(this.#queryParameters.TableName);
        return new Promise((resolve, reject) => {
            // this.#initializeParams(this.#queryParameters.TableName);
            let keys = Object.keys(body);
            Object.entries(this.#schema).map(([attribute, condition]) => {
                if (keys.indexOf(attribute) !== -1) {
                    keys.splice(keys.indexOf(attribute), 1);
                }
                // To validate if the required field is missing
                condition.required(body) && body[attribute] === undefined && reject(this.#errorHandler(attribute, body[attribute], condition.type.name.toLowerCase(), condition.validationErrorMessage));
                // To validating if the input and schema schema are of same type
                body[attribute] !== undefined && typeof (body[attribute]) !== condition.type.name.toLowerCase() && reject(this.#errorHandler(attribute, body[attribute], condition.type.name.toLowerCase(), condition.validationErrorMessage));
                // To validate if the field matches any additional condtion
                body[attribute] !== undefined && condition.validate && !condition.validate(body[attribute]) && reject(this.#errorHandler(attribute, body[attribute], condition.type.name.toLowerCase(), condition.validationErrorMessage));
                // append validate details to queryparameters. condition !condition.required(body) is to make sure the field is default
                // even if the value is specified
                let attributeValue = (body[attribute] === undefined || !condition.required(body)) ? condition.defaultValue : this.#typeConverter(condition.preferredInputFormat, body[attribute]);
                this.#generateParameters(id ? "UPDATE" : "CREATE", attribute, attributeValue, id);
            })
            //To support addition of attribute into table which is not specified in schema, but available in input data
            keys.map(key => this.#generateParameters(id ? "UPDATE" : "CREATE", key, body[key], id))
            resolve(this.#queryParameters);
        })
    }


    /**
     * Generate paraeters to get single item
     * @param {string} id 
     */
    getSingleItemQuery(id) {
        this.#queryParameters.Key = { guid: id };
        return this.#queryParameters;
    }

    /**
     * Generate parameters to get all items
     * @param {string} sortKey 
     * @param {number} limit 
     * @param {object} lastItem 
     */
    getAllItemsQuery(sortKey, limit, lastItem, projectionExpression) {
        try {
            if (limit && isNaN(Number(limit))) throw { name: "TypeError" };
            this.#queryParameters.Limit = limit;
            if (sortKey && sortKey !== "guid") {
                this.#queryParameters.IndexName = this.#schema[sortKey].GSI;
            }
            if (lastItem) {
                this.#queryParameters.ExclusiveStartKey = JSON.parse(lastItem);
            }
            if (projectionExpression) {
                this.#queryParameters.ProjectionExpression = projectionExpression;
            }
            return this.#queryParameters;
        }
        catch (error) {
            if (error.name === "SyntaxError") {
                error.statusCode = 422;
                error.message = "Invalid last item";
            }
            else if (error.name === "TypeError") {
                error.statusCode = 422;
                error.message = "Invalid sort(expecting string and one of the required fields) or limit(expecting number)";
            }
            throw error;
        }
    }

    /**
     * Generate parameters to delete single item
     * @param {string} id 
     */
    deleteItemQuery(id) {
        this.#queryParameters.Key = { guid: id };
        this.#queryParameters.ReturnValues = "ALL_OLD";
        return this.#queryParameters;
    }

    /**
     * To remove/convert fields that are filled with values while inserting into DB
     * @param {object (DynamoDB output)} queryResult 
     * @param {string} dataField 
     * @param {function} parser 
     * @param {string} order 
     * @param {number} limit 
     * @param {object} lastItem 
     * @param {boolean} sort
     * @param {array} idCollection
     * 
     */
    parseQueryResult(queryResult, dataField, parser, sort, idCollection) {
        return new Promise((resolve, reject) => {
            try {
                if(sort) { //since getbatch result is in random order, they are sorted after fetching by comparing with the id collecion
                    let idCollectionArray = idCollection.map(id => id.guid);
                    let sortedData = [];
                    queryResult[dataField].map(item => {
                        sortedData[idCollectionArray.indexOf(item.guid)] = item;
                    })
                    queryResult[dataField] = sortedData;
                }
                Array.isArray(queryResult[dataField]) ? queryResult[dataField].map(result => parser(result)) : parser(queryResult[dataField]);
                resolve({ [dataField]: queryResult[dataField], LastEvaluatedKey: queryResult.LastEvaluatedKey });
            }
            catch (error) {
                reject(error);
            }
        })
    }

    /**
     * To get data in sorted order. This method is used in DESC ordering
     * @param {object} queryResult 
     * @param {string} dataField 
     * @param {number} limit 
     * @param {object} lastItem 
     */
    getSortedData(queryResult, dataField, limit, lastItem) {
        return new Promise((resolve, reject) => {
            try {
                if (queryResult[dataField].length !== 0) {
                    queryResult[dataField].reverse();
                    limit = limit ? Number(limit) : 0;
                    lastItem = lastItem ? JSON.parse(lastItem) : lastItem;
                    //to prevent execution of find index more than once in a call, it is stored in a local var
                    let lastItemIndex = lastItem ? queryResult[dataField].findIndex(data => data.guid === lastItem.guid) : -1;
                    //start data
                    let start = lastItemIndex !== -1 ? lastItemIndex + 1 : 0;
                    //end data
                    let end = (start + limit > queryResult[dataField].length - 1) ? queryResult[dataField].length - start : limit;
                    if (start + end !== queryResult[dataField].length) {
                        queryResult.LastEvaluatedKey = { guid: queryResult[dataField][(start + end) - 1].guid };
                    }
                    queryResult[dataField] = queryResult[dataField].splice(start, end);
                    resolve({ Items: queryResult[dataField], LastEvaluatedKey: queryResult.LastEvaluatedKey });
                }
                else {
                    resolve({ Items: queryResult[dataField], LastEvaluatedKey: queryResult.LastEvaluatedKey });
                }
            }
            catch (error) {
                if(error.name === "SyntaxError"){
                    error.statusCode = 422;
                    error.message = "Invalid last item";
                }
                reject(error);
            }
        })
    }

    /**
     * To generate query for batch get operation
     * @param {array} idCollection 
     */
    getBatchItemQuery(idCollection) {
        let tableName = this.#queryParameters.TableName;
        this.#queryParameters = {};
        this.#queryParameters.RequestItems = {
            [tableName]: {
                Keys: idCollection
            }
        }
        return this.#queryParameters;
    }

    /**
     * To validate the patch request
     * @param {object} body 
     * @param {string} id 
     */
    validatePatch(body, id) {
        this.#initializeParams(this.#queryParameters.TableName);
        return new Promise((resolve, reject) => {
            try {
                Object.entries(body).map(([key, value]) => {
                    if (this.#schema[key]) {//validate only if the keys are available in schema
                        // To validate type
                        this.#schema[key].type.name.toLowerCase() !== typeof (value) && reject(this.#errorHandler(key, value, this.#schema[key].type.name.toLowerCase(), this.#schema[key].validationErrorMessage))
                        // To validate if the field matches any additional condtion
                        this.#schema[key].validate && !this.#schema[key].validate(value) && reject(this.#errorHandler(key, value, this.#schema[key].type.name.toLowerCase(), this.#schema[key].validationErrorMessage));
                        // To validate patch condition
                        this.#schema[key].patchValidation && !this.#schema[key].patchValidation(body) && reject(this.#errorHandler(key, value, this.#schema[key].type.name.toLowerCase(), this.#schema[key].patchValidationError))
                    }
                    this.#generateParameters("UPDATE", key, value, id);
                })
                resolve(this.#queryParameters);
            }
            catch (error) {
                reject(error);
            }
        })
    }
}

module.exports = Dynamo;