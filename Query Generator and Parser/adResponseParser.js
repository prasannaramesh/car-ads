/**
 * To remove the default values assigned for sorting convertion in database. 
 * Specified in a seperate file, since only one field needs to be checkeed and 
 * unnecessary iteration of schema can be avaioded
 * 
 * @param {Object} queryResult 
 */


module.exports.parser = (queryResult) => {
    if(queryResult.new) {
        delete queryResult.mileage;
        delete queryResult["first_registration"];
        return queryResult;
    }
    else {
        queryResult["first_registration"] = new Date(queryResult["first_registration"]).toJSON().split("T")[0];
        return queryResult;
    }
}