const chai = require("chai");
const chaiHttp = require("chai-http");
const { response } = require("express");
const app = require("../app");

chai.should();
chai.use(chaiHttp);




describe("Testing of all APIs without optional queries in GET", function () {

    const deleteInput = (guid, done) => {
        return chai.request(app)
            .delete(`/ads/${guid}`)
            .end((error, response) => {
                response.body.should.be.a("object");
                if (guid !== "errorId") {
                    response.should.have.status(200);
                    response.body.should.have.property("message").eq("Ad removed")
                }
                else {
                    response.should.have.status(404);
                    response.body.should.have.property("message").eq(`Ad not found for id: ${guid}`)
                }
                done();
            })
    }

    const getSingleAd = (guid, done, adContent) => {
        return chai.request(app)
            .get(`/ads/${guid}`)
            .end((error, response) => {
                response.body.should.be.a("object");
                if (guid !== "errorId") {
                    response.should.have.status(200);
                    response.body.should.have.property("guid").eq(guid)
                    Object.entries(adContent).map(([key, value]) => response.body.should.have.property(key).eq(value));
                }
                else {
                    response.should.have.status(404);
                    response.body.should.have.property("message").eq(`Ad not found for id: ${guid}`);
                }
                done();
            })
    }

    const patchAd = (guid, done, patchContent, adContent) => {
        return chai.request(app)
            .patch(`/ads/${guid}`)
            .send(patchContent)
            .end((error, response) => {
                response.body.should.be.a("object");
                if (guid !== "errorId") {
                    response.should.have.status(200);
                    response.body.should.have.property("guid").eq(guid)
                    Object.entries(adContent).map(([key, value]) => response.body.should.have.property(key).eq(value));
                }
                else {
                    response.should.have.status(404);
                    response.body.should.have.property("message").eq(`Ad not found for id: ${guid}`);
                }
                done();
            })
    }

    const putAd = (guid, done, putContent, adContent) => {
        return chai.request(app)
            .put(`/ads/${guid}`)
            .send(putContent)
            .end((error, response) => {
                response.body.should.be.a("object");
                if (guid !== "errorId") {
                    response.should.have.status(200);
                    response.body.should.have.property("guid").eq(guid);
                    Object.entries(adContent).map(([key, value]) => response.body.should.have.property(key).eq(value));
                }
                else {
                    response.should.have.status(404);
                    response.body.should.have.property("message").eq(`Ad not found for id: ${guid}`)
                }
                done();
            })
    }

    let testInputCollection = [
        {
            create: { title: "Audi", price: 1000, fuel: "gasoline", new: false, mileage: 2000, first_registration: "2010-02-28" },
            update: { title: "Audi", price: 1000, fuel: "gasoline", new: false, mileage: 2000, first_registration: "2010-02-28", owners: "1 previous owner", email: "user1@gmail.com" },
            patch: { price: 800 }
        },
        {
            create: { title: "BMW", price: 3000, fuel: "diesel", new: true },
            update: { title: "BMW", price: 3000, fuel: "diesel", new: true, telephone: "1234567890" },
            patch: { telephone: "123456789" }
        },
    ];

    let guid = [];


    /**
     * POST route testing
     */
    testInputCollection.map((input, index) => {
        it(`Test ${index} - POST /ads`, (done) => {
            chai.request(app)
                .post("/ads")
                .send(input.create)
                .end((error, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a("object");
                    response.body.should.have.property("guid");
                    guid.push(response.body.guid);
                    Object.entries(input.create).map(([key, value]) => response.body.should.have.property(key).eq(value));
                    done();
                })
        })
    })

    /**
     * PUT route testing
     */
    testInputCollection.map((input, index) => {
        it(`Test ${index} - PUT /ads/:id`, (done) => {
            putAd(guid[index], done, input.update, input.update);
        })
    })

    /**
     * PATCH route testing
     */
    testInputCollection.map((input, index) => {
        it(`Test ${index} - PATCH /ads/:id`, (done) => {
            patchAd(guid[index], done, input.patch, { ...input.update, ...input.patch });
        })
    })

    /**
     * Get single Ad
     */

    testInputCollection.map((input, index) => {
        it(`Test ${index} - GET single Ad /ads/:id`, (done) => {
            getSingleAd(guid[index], done, { ...input.update, ...input.patch })
        })
    })



    /**
     * GET all route testing
     */
    it(`Test: get all Ads - GET  /ads`, (done) => {
        chai.request(app)
            .get(`/ads`)
            .end((error, response) => {
                response.should.have.status(200);
                response.body.should.be.a("object");
                response.body.Items.length.should.be.eq(testInputCollection.length)
                done();
            })
    })

    /**
     * DELETE route testing
     */
    testInputCollection.map((input, index) => {
        it(`Test ${index} - DELETE /ads/:id`, (done) => {
            deleteInput(guid[index], done)
        })
    })

    /**
     * GET single AD for invalid id/id not available in the databse
     */
    it(`Test: Error for invalid Ad Id - GET  /ads/:id`, (done) => {
        getSingleAd("errorId", done);
    })

    /**
     * DELETE single AD for invalid id/id not available in the databse
     */
    it(`Test: Error for invalid Ad Id - Delete  /ads/:id`, (done) => {
        deleteInput("errorId", done);
    })

    /**
    * PUT an AD for invalid id/id not available in the databse
    */
    it(`Test: Error for invalid Ad Id - PUT  /ads/:id`, (done) => {
        putAd("errorId", done, { extra: "add" });
    })

    /**
    * PATCH an AD for invalid id/id not available in the databse
    */
    it(`Test: Error for invalid Ad Id - PATCH  /ads/:id`, (done) => {
        patchAd("errorId", done, testInputCollection[0].update);
    })
})
