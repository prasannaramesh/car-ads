const chai = require("chai");
const chaiHttp = require("chai-http");
const { response } = require("express");
const app = require("../app");

chai.should();
chai.use(chaiHttp);



let testInputCollection = [
    { title: "BMW", price: 23000, fuel: "diesel", new: true }, //0
    { title: "Audi", price: 2000, fuel: "gasoline", new: false, mileage: 23, first_registration: "2014-02-28" },//1
    { title: "Ford", price: 870, fuel: "gasoline", new: false, mileage: 17, first_registration: "2010-02-28" }, //2
    { title: "Benz", price: 2300, fuel: "diesel", new: false, mileage: 23, first_registration: "2010-02-28" }, //3
    { title: "Renault", price: 16000, fuel: "gasoline", new: true }, //4
];

let guid = ["AS-20201229113902466", "AS-20201229113902191", "AS-20201229113901979", "AS-20201229113902098", "AS-20201229113902367", "AS-20201229113902274"];


describe("API Testing for GET all ads with optional parameters", () => {


    testInputCollection.map((input, index) => {
        it(`Test ${index} - POST /ads`, (done) => {
            chai.request(app)
                .post("/ads")
                .send(input)
                .end((error, response) => {
                    response.should.have.status(201);
                    response.body.should.be.a("object");
                    response.body.should.have.property("guid");
                    guid.push(response.body.guid);
                    Object.entries(input).map(([key, value]) => response.body.should.have.property(key).eq(value));
                    done();
                })
        })
    })

})