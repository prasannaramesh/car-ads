const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
const DynamoHandler = require("../Query Generator and Parser/Dynamo");
const schema = require("../Model/Ad");

chai.use(chaiAsPromised);

describe('Request body validation Test', function () {

    /**
     * Test schema field against body to check if it has valid inputs
     */
    describe("Validation used in CREATE and PUT", function () {
        const testCases = [
            //new cars
            { name: "Eroor for missing fields (fuel, price,new)", input: { title: "name" }, type: "reject" },
            { name: "Eroor for missing fields (price,new)", input: { title: "name", fuel: "gasoline" }, type: "reject" },
            { name: "Eroor for missing field (new)", input: { title: "name", fuel: "gasoline", price: 1000 }, type: "reject" },
            { name: "Resolve when all the required attributes are present for new car", input: { title: "name", price: 1000, fuel: "gasoline", new: true}, type: "resolve" },
            { name: "Error for title is not of string type", input: { title: 2000, price: 1000, fuel: "gasoline", new: true }, type: "reject" },
            { name: "Error for fuel types not in the allowed types", input: { title: "name", price: 1000, fuel: "dd", new: true }, type: "reject" },
            { name: "Error for price not of number type", input: { title: "name", price: "1000", fuel: "gasoline", new: true }, type: "reject" },
            { name: "Error for new not of bool type", input: { title: "name", price: 1000, fuel: "gasoline", new: "true" }, type: "reject" },
            //old cars
            { name: "Error for missing mileage and first_registration for old vehichles", input: { title: "name", price: 1000, fuel: "gasoline", new: false }, type: "reject" },
            { name: "Error for missing mileage for old vehichles", input: { title: "name", price: 1000, fuel: "gasoline", new: false, first_registration: "2020-02-03" }, type: "reject" },
            { name: "Error for missing first registration for old vehichles", input: { title: "name", price: 1000, fuel: "gasoline", new: false, first_registration: "2020-02-03" }, type: "reject" },
            { name: "Error for first_registration not in the format yyyy-mm-dd", input: { title: "name", price: 1000, fuel: "gasoline", new: false, mileage: 10, first_registration: "" }, type: "reject" },
            { name: "Error for invalid date in first_registration", input: { title: "name", price: 1000, fuel: "gasoline", new: false, mileage: 10, first_registration: "2020-02-30" }, type: "reject" },
            { name: "Resolve when all the required attributes are present", input: { title: "name", price: 1000, fuel: "gasoline", new: false, mileage: 10, first_registration: "2020-02-28" }, type: "resolve" },
            { name: "Resolve when all the required attributes are present along with additional attributes", input: { title: "name", price: 1000, fuel: "gasoline", new: false, mileage: 10, first_registration: "2020-02-28", comment: "Just a sample comment", sold: true }, type: "resolve" },
        ];
        testCases.map(testCase => {
            it(testCase.name, async function () {
                let dynamoHandler = new DynamoHandler("", schema);
                if (testCase.type === "reject") {
                    return chai.expect(dynamoHandler.validate(testCase.input)).to.eventually.be.rejected;
                }
                else {
                    return chai.expect(dynamoHandler.validate(testCase.input)).to.eventually.be.fulfilled
                        .then((data) => Object.entries(testCase.input).map(([key, value]) => key === "first_registration" ? chai.expect(data.Item[key]).to.equal(new Date(value).getTime()) : chai.expect(data.Item[key]).to.equal(value)));
                }
            })
        })
    })

    describe("Validation used in PATCH", function () {
        const testCases = [
            { name: "Mismatch in type of the field", input: { title: 22 }, type: "reject" },
            { name: "Error while updating without mileage and first_registration for old cars", input: { new: false }, type: "reject" },
            { name: "Error when the mileage is not of number type for old cars", input: { new: false, mileage: "200", first_registration: "2020-02-20" }, type: "reject" },
            { name: "Resovle for PATCH on switch to old car when mileage and first_registration are defined", input: { new: false, mileage: 200, first_registration: "2020-02-20" }, type: "resolve" },
            { name: "Resovle for PATCH on mileage alone", input: {mileage: 3000}, type: "resolve" },
            { name: "Resovle for PATCH on switch from old car to new", input: { new: true }, type: "resolve" },
            { name: "Resovle non required attribute PATCH", input: { extra: "Electronic parking brakes" }, type: "resolve" }
        ];
        testCases.map(testCase => {
            it(testCase.name, async function () {
                let dynamoHandler = new DynamoHandler("", schema);
                if (testCase.type === "reject") {
                    return chai.expect(dynamoHandler.validatePatch(testCase.input, "id")).to.eventually.be.rejected;
                }
                else {
                    return chai.expect(dynamoHandler.validatePatch(testCase.input, "id")).to.eventually.be.fulfilled;
                }
            });
        })
    })
})