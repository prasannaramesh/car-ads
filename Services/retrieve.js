const AWS = require("../AWS SDK/AWS");
const config = require("../AWS SDK/config");
const enums = require("../enums");
const DynamoHandler = require("../Query Generator and Parser/Dynamo");
const schema = require("../Model/Ad");
const { parser } = require("../Query Generator and Parser/adResponseParser");

//To retrieve all ads
module.exports.retrieveAllAds = async (request, response) => {
    try {
        let { limit, sort, lastItem, order } = request.query, scanLimit;
        let dynamoHandler = new DynamoHandler(config.tableName, schema), data;
        let lastScannedItem = order === "DESC" ? null : lastItem;
        // For DESC order, only guid is projected to elimate 1MB scan limit. The projected guids are limited by default and then batch get is applied
        scanLimit = order === "DESC" ? null : (limit ? (limit > enums.dynamoDBMaxScanLimit ? enums.dynamoDBMaxScanLimit : limit) : enums.dynamoDBDefaultScanLimit);
        let query = dynamoHandler.getAllItemsQuery(sort, scanLimit, lastScannedItem, order === "DESC" && "guid");
        let queryResult = await AWS.documentClient.scan(query).promise();
        if (order === "DESC") { // when the order is desc
            lastScannedItem = lastItem;
            scanLimit = limit ? limit : enums.dynamoDBDefaultScanLimit;
            let { Items: idCollection, LastEvaluatedKey } = await dynamoHandler.getSortedData(queryResult, "Items", scanLimit, lastScannedItem);
            if (idCollection.length !== 0) { //when there is entry in the table
                // Batch get allows 16 MB by allowing size in multiples of 4kb for the output. Though this is not effecient, it reduces the cost 
                // on multiple seperate gets
                let batchData = await AWS.documentClient.batchGet(dynamoHandler.getBatchItemQuery(idCollection)).promise();
                batchData = { Items: batchData.Responses[config.tableName], LastEvaluatedKey: LastEvaluatedKey };
                data = await dynamoHandler.parseQueryResult(batchData, "Items", parser, true, idCollection)
            }
            else { //when there is no entry in the data
                data = { Items: [] }
            }
        }
        else {
            data = await dynamoHandler.parseQueryResult(queryResult, "Items", parser);
        }
        response.status(200).json(data);
    }
    catch (error) {
        response.status(error.statusCode || 500).json({
            name: error.name,
            message: error.message
        })
    }
}

//To retrieve single ad specified by id
module.exports.retrieveSingleAd = async (request, response) => {
    try {
        let dynamoHandler = new DynamoHandler(config.tableName, schema);
        let query = dynamoHandler.getSingleItemQuery(request.params.id);
        let queryResult = await AWS.documentClient.get(query).promise();
        if (queryResult.Item) {
            let data = await dynamoHandler.parseQueryResult(queryResult, "Item", parser);
            response.status(200).json(data.Item);
        }
        else {
            response.status(404).json({ message: `Ad not found for id: ${request.params.id}` })
        }
    }
    catch (error) {
        response.status(error.statusCode || 500).json({
            name: error.name,
            message: error.message
        })
    }
}