const AWS = require("../AWS SDK/AWS");
const config = require("../AWS SDK/config");
const QueryGenerator = require("../Query Generator and Parser/Dynamo");


// To delete an ad 
module.exports.deleteAd = async (request, response) => {
    try {
        let queryGenerator = new QueryGenerator(config.tableName);
        let params = await queryGenerator.deleteItemQuery(request.params.id);
        let data = await AWS.documentClient.delete(params).promise();
        if (data.Attributes) {
            response.status(200).json({ message: "Ad removed" })
        }
        else {
            response.status(404).json({ message: `Ad not found for id: ${request.params.id}` })
        }
    }
    catch (error) {
        response.status(error.statusCode || 500).json({
            name: error.name,
            message: error.message
        })
    }
}