const DynamoHandler = require("../Query Generator and Parser/Dynamo");
const schema = require("../Model/Ad");
const config = require("../AWS SDK/config");
const AWS = require("../AWS SDK/AWS")
const { parser } = require("../Query Generator and Parser/adResponseParser")

//Ad creator
module.exports.createAd = async (request, response) => {
    try {
        let dynamoHandler = new DynamoHandler(config.tableName, schema);
        let params = await dynamoHandler.validate(request.body)
        await AWS.documentClient.put(params).promise();
        let data = await dynamoHandler.parseQueryResult(params, "Item", parser);
        response.status(201).send(data.Item);
    }
    catch (error) {
        response.status(error.statusCode || 500).json({
            name: error.name,
            message: error.message
        })
    }
}