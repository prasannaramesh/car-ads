const AWS = require("../AWS SDK/AWS");
const config = require("../AWS SDK/config");
const DynamoHandler = require("../Query Generator and Parser/Dynamo");
const schema = require("../Model/Ad");
const { parser } = require("../Query Generator and Parser/adResponseParser");
const { retrieveSingleAd } = require("./retrieve");

// To update ad specified by the id
module.exports.updateAd = async (request, response) => {
    try {
        let { guid, ...body } = request.body;
        let dynamoHandler = new DynamoHandler(config.tableName, schema);
        let dataAvailable = await AWS.documentClient.get(dynamoHandler.getSingleItemQuery(request.params.id)).promise();
        if (dataAvailable.Item) {
            let query = await dynamoHandler.validate(body, request.params.id);
            let queryResult = await AWS.documentClient.update(query).promise()
            let data = await dynamoHandler.parseQueryResult(queryResult, "Attributes", parser);
            response.status(200).json(data.Attributes);
        }
        else {
            response.status(404).json({ message: `Ad not found for id: ${request.params.id}` })
        }
    }
    catch (error) {
        response.status(error.statusCode || 500).json({
            name: error.name,
            message: error.message
        })
    }
}


//To update specified attribute of an ad
module.exports.updateAdAttributes = async (request, response) => {
    try {
        let { guid, ...body } = request.body;
        if (body["new"]) {
            body["mileage"] = 0; body["first_registration"] = "-";
        }
        let dynamoHandler = new DynamoHandler(config.tableName, schema);
        let dataAvailable = await AWS.documentClient.get(dynamoHandler.getSingleItemQuery(request.params.id)).promise();
        if (dataAvailable.Item) {
            let query = await dynamoHandler.validatePatch(body, request.params.id);
            let queryResult = await AWS.documentClient.update(query).promise();
            let data = await dynamoHandler.parseQueryResult(queryResult, "Attributes", parser);
            response.status(200).json(data.Attributes);
        }
        else {
            response.status(404).json({ message: `Ad not found for id: ${request.params.id}` });
        }
    }
    catch (error) {
        response.status(error.statusCode || 500).json({
            name: error.name,
            message: error.message
        })
    }
}