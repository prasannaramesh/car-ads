const AWS = require('../AWS SDK/AWS')
const config = require('../AWS SDK/config')
var dynamoDB = AWS.dynamoDB;

/**
 * Creation of parameters for table generation.
 * Multiple GlobalSecondaryIndexes on all the specified attributes, to support sorting on all the fields.
 * Reduction of global secondary index by sorting new car field from first registration index
 */
var params = {
    TableName: config.tableName,
    KeySchema: [
        { AttributeName: "guid", KeyType: "HASH" },
    ],
    AttributeDefinitions: [
        { AttributeName: "guid", AttributeType: "S" },
        { AttributeName: "title", AttributeType: "S" },
        { AttributeName: "fuel", AttributeType: "S" },
        { AttributeName: "price", AttributeType: "N" },
        { AttributeName: "mileage", AttributeType: "N" },
        { AttributeName: "first_registration", AttributeType: "N" }, // to get data in sorted
    ],
    GlobalSecondaryIndexes: [
        {
            IndexName: "title_index",
            KeySchema: [
                { AttributeName: "title", KeyType: "HASH" },
            ],
            Projection: {
                ProjectionType: "ALL"
            },
            ProvisionedThroughput: {
                ReadCapacityUnits: 300,
                WriteCapacityUnits: 100
            }
        },
        {
            IndexName: "fuel_index",
            KeySchema: [
                { AttributeName: "fuel", KeyType: "HASH" },
            ],
            Projection: {
                ProjectionType: "ALL"
            },
            ProvisionedThroughput: {
                ReadCapacityUnits: 300,
                WriteCapacityUnits: 100
            }
        },
        {
            IndexName: "price_index",
            KeySchema: [
                { AttributeName: "price", KeyType: "HASH" },
            ],
            Projection: {
                ProjectionType: "ALL"
            },
            ProvisionedThroughput: {
                ReadCapacityUnits: 300,
                WriteCapacityUnits: 100
            }
        },
        {
            IndexName: "mileage_index",
            KeySchema: [
                { AttributeName: "mileage", KeyType: "HASH" },
            ],
            Projection: {
                ProjectionType: "ALL"
            },
            ProvisionedThroughput: {
                ReadCapacityUnits: 300,
                WriteCapacityUnits: 100
            }
        },
        {
            IndexName: "first_registration_index",
            KeySchema: [
                { AttributeName: "first_registration", KeyType: "HASH" },
            ],
            Projection: {
                ProjectionType: "ALL"
            },
            ProvisionedThroughput: {
                ReadCapacityUnits: 300,
                WriteCapacityUnits: 100
            }
        },
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 300,
        WriteCapacityUnits: 100
    }
}

dynamoDB.createTable(params, (error, data) => {
    if (error) {
        console.error(`Error: ${JSON.stringify(error, null, 2)}`)
    }
    else {
        console.log(JSON.stringify(data, null, 2))
    }
})