# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Setup Guide
* API details

### How do I get set up? ###

Local dynamoDB along with the JAR files has been added to the repository. Navigate to the /dynamodb_local, and run the command "java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb" and then start the node server

If an another local dynamoDB (other than the one in this repository) is used, make sure the DB is running with the above mentioned command. Then navigate to the root folder (folder in which app.js is present) and run the following command 
"node '.\Table creator\createTable.js'". 

Run "npm install" to install all the dependencies and type "npm start" to start the server.


If no port is specified in .env file, by default server will start in the port 3000. Along with this a local DynamoDB GUI will be available at the port 8001. This can be changed from app.js file.

### API Details ###

* Create Ad		-	POST	- /ads
* Update Ad		-	PUT		- /ads/:id
* Update Ad		-	PATCH	- /ads/:id
* Get Ad		-	GET		- /ads/:id
* Get Ads(all)	-	GET		- /ads (with optional query parameters)
* Delete Ad		-	DELETE	- /ads/:id

A documentation on all the API has been made with Postman and is available here -> https://documenter.getpostman.com/view/9737859/TVsyfkZv