const express = require("express");
const bodyParser = require("body-parser")
const cors = require("cors");
const { createAd } = require("./Services/createAd");
const { retrieveAllAds, retrieveSingleAd } = require("./Services/retrieve");
const { updateAd, updateAdAttributes } = require("./Services/updateAd");
const { deleteAd } = require("./Services/deleteAd");
require("dotenv/config")
const app = express();

//dynamo db admin GUI
if (process.env.NODE_ENV === "local") {
    const { createServer } = require('dynamodb-admin');
    const AWS = require('./AWS SDK/AWS')
    const dynamoDBAdmin = createServer(AWS.dynamoDB, AWS.documentClient);
    const port = 8001;
    const server = dynamoDBAdmin.listen(port);
    server.on('listening', () => {
        const address = server.address();
        console.log(`listening on http://0.0.0.0:${address.port}`);
    });
}


// Middlewares
app.use(cors());
app.use(bodyParser.json());


// Routes
app.post("/ads", createAd);
app.get("/ads", retrieveAllAds);
app.get("/ads/:id", retrieveSingleAd);
app.put("/ads/:id", updateAd);
app.patch("/ads/:id", updateAdAttributes);
app.delete("/ads/:id", deleteAd);

// Server
const port = process.env.PORT || 3000;
module.exports = app.listen(port, () => console.log(`App started. Listening to port ${port}`));