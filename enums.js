//enums


module.exports = {
    // Allowed fuel types in request
    fuelTypes: ["gasoline", "diesel"],
    // Default scan limit
    dynamoDBDefaultScanLimit: 10,
    // Maximum scan limit
    dynamoDBMaxScanLimit: 50
}