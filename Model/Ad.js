const enums = require("../enums")

/**
 * Specifies the schema to validate the input
 * {{
 *      type: desired format of the key
 *      required: if the field is mandatory or optional
 *      valiadte: if there is any additional validation to be performed on the key
 *      prefferedInputFormat: the format in which the field has to written in database
 *      prefferedOutputFormat: the format in which the field has to be sent. This is optional. If this is empty.
 *      deafultValue: Specifies if the field has any default values incase it's not required.
 *      GSI: specifies global second index for sorted data retrieval
 *      patchValidation: specifies the condition while patch operation
 * }}
 */
module.exports = {
    title: {
        type: String,
        required: () => true,
        GSI: "title_index"
    },
    fuel: {
        type: String,
        required: () => true,
        validate: (fuelType) => enums.fuelTypes.indexOf(fuelType) !== -1,
        validationErrorMessage: `Expecting one from ${enums.fuelTypes}.`,
        GSI: "fuel_index"
    },
    price: {
        type: Number,
        required: () => true,
        GSI: "price_index"
    },
    new: {
        type: Boolean,
        required: () => true,
        GSI: "first_registration_index",
        patchValidation: (body) => {
            if (!body["new"]) {
                return body["mileage"] !== undefined && body["first_registration"] !== undefined;
            }
            else {
                return true;
            }
        },
        patchValidationError: "Also, Mileage and first registration fields are required for old cars"
    },
    mileage: {
        type: Number,
        required: (requestBody) => !requestBody["new"],
        defaultValue: Number.MAX_SAFE_INTEGER,
        GSI: "mileage_index"
    },
    first_registration: {
        type: String,
        required: (requestBody) => !requestBody["new"],
        validate: (date) => {
            let dateValue = date || "";
            //Reference for regex: https://stackoverflow.com/questions/22061723/regex-date-validation-for-yyyy-mm-dd
            let regex = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/g;
            return !(dateValue.match(regex) === null || isNaN(new Date(dateValue)) || (new Date(dateValue).getTime() > new Date().getTime()) 
                    || (!isNaN(new Date(dateValue)) && new Date(date).toISOString().split("T")[0] !== date)
            );
        },
        preferredInputFormat: Date,
        validationErrorMessage: "Expecting in the format (yyyy-mm-dd).",
        defaultValue: Number.MAX_SAFE_INTEGER,
        GSI: "first_registration_index"
    },
};