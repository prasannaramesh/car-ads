const aws_sdk = require("aws-sdk");
const config = require("./config");
require("dotenv/config");

aws_sdk.config.update( process.env.NODE_ENV === "local" ? config.local : config.remote);

const dynamoDB = new aws_sdk.DynamoDB({apiVersion: '2012-08-10'});

const documentClient = new aws_sdk.DynamoDB.DocumentClient();

module.exports = {dynamoDB, documentClient};